# T-303-HUGB 2019 Group 7 - Woofer inc.

Authors and acknowledgment

* Þorvaldur Breki Böðvarsson
* Guðni Natan Gunnarsson
* Kormákur Breki Gunnlaugsson
* Ragnar Geir Ragnarsson
* Pálmi Chanachai Rúnarsson
* Valur Hólm Sigurðarson

# How to run Woofer

### Repository Service
1. Install python 3.7+
2. Download or clone the latest woofer build
3. Navigate to the T-303-HUGB-2019-Group-7 folder in a terminal of your choice
4. Use this command: `python -m src.app.repository`
5. It should say something like:
>  Starting repository service!  
Listening on ws://localhost:8765  
6. You have started the repository service!

### Controller Service
1. Install python 3.7+
2. Download or clone the latest woofer build
3. Start the repository service
3. Navigate to the T-303-HUGB-2019-Group-7 folder in a terminal of your choice
4. Use this command: `python -m src.app.controller`
5. It should say something like:
>  Connected to repository at ws://localhost:8765  
Starting controller service!  
Listening on ws://localhost:8766  
6. You have started the controller service!


### UI Client
_Coming soon..._